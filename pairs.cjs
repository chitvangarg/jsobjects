function pairs(obj){
    let pairs = [];
    if(obj != null){
        for(let key in obj){
            pairs.push([key, obj[key]])
        }
    }

    return pairs;
}

module.exports = pairs;