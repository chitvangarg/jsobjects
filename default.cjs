function defaults(obj, defaultProps){
    return {...obj, ...defaultProps};
}

module.exports = defaults